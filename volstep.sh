#!/bin/bash

if [[ $1 = "up" ]]
then
	amixer -D pulse sset Master 2%+
	exit 0
fi

if [[ $1 = "down" ]]
then
	amixer -D pulse sset Master 2%-
	exit 0
fi

exit 1
