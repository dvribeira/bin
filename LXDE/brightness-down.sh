#!/bin/bash

xbacklight -5

curr_brightness=$(xbacklight -get)

notify-send "Diminuir brilho:" "Brilho: $curr_brightness" -i video-display -t 1000
