#!/bin/bash

#Change the following parameters to your needs
LOG_DIR=~/transmission-finished-script
TO_APPEND=/finalizados # where to put the completely finished and shared downloads, this is appended to the actual location of the download
USER=transmissionusername
USER2=transmissionpass
GOAL_RATIO=3 #which sharing ratio are we aiming for in transmission?

E_XCD=86       # Can't change directory?

cd $LOG_DIR || {
   echo "Cannot change to necessary directory." >&2
   exit $E_XCD;
}


torrent_list() {
	transmission-remote -n ${USER}:${USER2} -l | awk -F'^ +|  +' 'NF>9{print $2,($9=="Finished"),$8+0,$10}' |
    sort -n -k2,2r
}

while read id finished ratio name; do
	if ((finished)); then
		echo "$name is finished."
		
		if echo "$ratio >= $GOAL_RATIO" | bc; then
			echo "$name has a ratio of at least $GOAL_RATIO and it is gonna get processed."

			torrent_info() {
				transmission-remote -n ${USER}:${USER2} -t $id -i | grep -e Location
			}

			while read heading location; do
				#if echo $location | grep -q "sickbeard"; then
				#	echo "$location matches 'sickbeard'"
					date >> log.txt
					echo -n "[User] " >> log.txt
					whoami >> log.txt
					echo -n "[Torrent Name] " >> log.txt
					echo $name >> log.txt
					echo -n "[Torrent Location] " >> log.txt
					echo $location >> log.txt
					dest_path=$location$TO_APPEND
					echo "moving to $dest_path"
					echo "[Torrent Destination] $dest_path" >> log.txt
					transmission-remote -n ${USER}:${USER2} -t $id --move $dest_path >> log.txt
					transmission-remote -n ${USER}:${USER2} -t $id --remove >> log.txt
					echo "Done."
				#else
				#	echo "$location does not match 'sickbeard'. Skipped."
				#fi
			done < <(torrent_info)
		else
			echo "$name has finished but has not a ratio of at least $GOAL_RATIO. Skipping it."
		fi
	else
		echo "$name is not finished. Exiting."
		break
	fi
done < <(torrent_list)

echo "Finished script."
exit
