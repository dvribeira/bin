#!/bin/bash

# Usage:
# $0 max_num_of_concurrent_jobs command files...

max_num_of_concurrent_jobs=$1;
cmd=$2
total_num_of_jobs=$#

#echo $total_num_of_jobs

while [ $total_num_of_jobs -gt 2 ]; do
    for ((c=0;c<max_num_of_concurrent_jobs;++c)); do
	($cmd ${!total_num_of_jobs}) &
    	((--total_num_of_jobs))
    	[ $total_num_of_jobs -gt 0 ]  || break
    done
    wait
done

